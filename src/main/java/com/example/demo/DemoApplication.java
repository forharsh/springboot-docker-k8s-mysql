package com.example.demo;

import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepository;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@SpringBootApplication
@RestController
public class DemoApplication {
    
    @Autowired
    Environment environment;

    @Autowired
	StudentRepository studentRepository;

	@GetMapping("/")
	List<Student> home() {
	    /*String port = environment.getProperty("local.server.port");
		return "Hello Vijay ! Spring is here! and port = " + port;*/
		return studentRepository.findAll();
	}

	@PostMapping(value = "save")
	public ResponseEntity<?> save(@RequestBody final Student student) {
		return new ResponseEntity<>(studentRepository.save(student), HttpStatus.CREATED);
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}