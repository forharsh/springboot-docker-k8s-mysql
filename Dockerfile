FROM openjdk:8-jre-alpine
VOLUME /tmp
ARG JAR_FILE
#ARG TIMATIC_API_ACCESS_DETAILS
RUN echo ${JAR_FILE}
#RUN echo ${TIMATIC_API_ACCESS_DETAILS}
ADD ${JAR_FILE} app.jar
#ADD target/demo-0.0.1-SNAPSHOT-exec.jar app.jar
#ENV TIMATIC_API_ACCESS ${TIMATIC_API_ACCESS_DETAILS}
ENTRYPOINT ["java", "-Xmx512m", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]
