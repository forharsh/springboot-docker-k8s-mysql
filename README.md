## Spring-Boot-Kubernetes-MySQL

Sample project to use spring boot application with mysql database in kubernetes.


- Start mysql database

```sh

$ kubectl create -f deployment/mysql-deployment.yaml

```

- Connect to mysql database

```sh
$ kubectl run -it --rm --image=mysql:5.7 --restart=Never mysql-client -- mysql -h mysql -ppassword
```
- Create table and insert some records

```sql
mysql> GRANT ALL PRIVILEGES ON test.* To 'testuser'@'%' IDENTIFIED BY 'test123';
mysql> FLUSH PRIVILEGES;

mysql> use test;
mysql>  CREATE TABLE `student` (   `id` int(11) NOT NULL,   `name` varchar(45) DEFAULT NULL,   `departmentName` varchar(45) DEFAULT NULL,   `department_name` varchar(255) DEFAULT NULL,   PRIMARY KEY (`id`) ) ENGINE=InnoDB;
mysql> insert into student values (2,'anu','QA','QA');

```

- Build application and deploy in kubernetes
- Create jar of application
```
mvn clean package
```
- Create docker image and push to docker hub
```
docker build  --build-arg JAR_FILE='target/springboot-docker-k8s-mysql-0.0.1-SNAPSHOT-exec.jar' -t forharsh/springboot-k8s-mysql:v1 .
docker push forharsh/springboot-k8s-mysql:v1 
```
- Deploy application into kubernetes cluster
```
kubectl create -f deployment/kubernetes.yaml
```
- Check Deployments
```
kubectl get deployments
```
- Check services
```
kubectl get svc
```
- Check pods
```
kubectl get pods
```
- Get Public url
```
minikube service travel-planner-backend-ports --url
```
- Rolling Updates
```
kubectl set image deployment/spring-backend-deployment  spring-backend=forharsh/springboot-k8s-mysql:v1
```

